package org.raf.rafmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RafmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(RafmailApplication.class, args);
	}

}
